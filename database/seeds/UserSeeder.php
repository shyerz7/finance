<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole 		= Role::where('name','admin')->first();
        $bdRole 		= Role::where('name','bd')->first();
        $operationRole 	= Role::where('name','operation')->first();
        $financeRole 	= Role::where('name','finance')->first();

        $userAdmin = new User();
        $userAdmin->id 	= 1;
        $userAdmin->name 	= 'admin';
        $userAdmin->email 	= 'admin@rad-research.com';
        $userAdmin->password 	= bcrypt(123);
        $userAdmin->attachRole($adminRole);

        $userBd = new User();
        $userBd->id 	= 2;
        $userBd->name 	= 'bd';
        $userBd->email 	= 'bd@rad-research.com';
        $userBd->password 	= bcrypt(123);
        $userBd->attachRole($bdRole);

        $userOperation = new User();
        $userOperation->id 	= 3;
        $userOperation->name 	= 'operation';
        $userOperation->email 	= 'operation@rad-research.com';
        $userOperation->password 	= bcrypt(123);
        $userOperation->attachRole($operationRole);

        $userFinance = new User();
        $userFinance->id  	= 4;
        $userFinance->name 	= 'finance';
        $userFinance->email 	= 'finance@rad-research.com';
        $userFinance->password 	= bcrypt(123);
        $userFinance->attachRole($financeRole);


    }
}
