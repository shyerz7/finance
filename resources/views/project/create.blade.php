@extends('layouts.master')

@section('css')
  <link rel="stylesheet" href="http://plugins.krajee.com/assets/6facb0ab/css/fileinput.min.css">
@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
      <h2>Create Project</h2>
      <ol class="breadcrumb">
  			<li>
            <a href="index-2.html">Index</a>
        </li>
        <li>
          	Create Project
       	</li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

     <form role="form" method="post" action="{{ route('project.store') }}"  enctype="multipart/form-data">

      <div class="col-lg-12">
       <div class="ibox float-e-margins">
          <div class="ibox-title">
             <h5>Create Project<small> Fill the blank input type</small></h5>
          </div>

           {{ csrf_field() }}
          <div class="ibox-content">
             <div class="row">
                <div class="col-sm-6 b-r">


                      <div class="form-group">
                        <label>Project Name</label>
                        <input type="text" placeholder="Inputkan Nama Projek" name="nama_project" class="form-control" required="required">
                      </div>

                      <div class="form-group">
                        <label>Tanggal Commisioned</label>
                        <input type="date"  name="created_at" class="form-control" required="required">
                      </div>

                      <div class="form-group">
                        <label>Client Name</label>
                        <input type="text" placeholder="Inputkan client_name" name="client_name" class="form-control" required="required">
                      </div>

                      <div class="form-group">
                        <label>Client Contact</label>
                        <input type="text" placeholder="Inputkan client_contact" name="client_contact" class="form-control" required="required">
                      </div>

                      <div class="form-group">
                        <label>Project Manajer</label>
                        <input type="text" placeholder="Inputkan project_manajer" name="project_manajer" class="form-control" required="required">
                      </div>

                      <div class="form-group">
                        <label>Project Method</label>
                        <input type="text" placeholder="Inputkan metode_project" name="metode_project" class="form-control" required="required">
                      </div>

                      <div class="form-group">
                        <label>Nilai Project</label>
                        {{-- <input type="text" id="value" name="value" pattern="[0-9]*" class="form-control"> --}}
                        <div class="input-group m-b">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="nilaiproject" name="nilai_project"  class="form-control" required="required">
                        </div>
                      </div>




                      <div>
                         <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Save</strong></button>
                         <button class="btn btn-sm btn-default pull-right m-t-n-xs" type="submit"><strong>Cancel</strong></button>
                      </div>

                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Desc</label>
                        <textarea name="desc" id="" cols="30" rows="10" class="form-control"></textarea>
                      </div>

                    <label class="control-label">Select File</label>
                    <input id="files" name="files[]" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" required="required">
                </div>

             </div>

          </div>
       </div>
    </div>

    </form>

</div>

@endsection

@section('js')
  <script src="http://plugins.krajee.com/assets/6facb0ab/js/fileinput.min.js"></script>
  <script type="text/javascript">

  	/* Tanpa Rupiah */
  	var tanpa_rupiah = document.getElementById('nilaiproject');
  	tanpa_rupiah.addEventListener('keyup', function(e)
  	{
  		tanpa_rupiah.value = formatRupiah(this.value);
  	});

  	/* Fungsi */
  	function formatRupiah(angka, prefix)
  	{
  		var number_string = angka.replace(/[^,\d]/g, '').toString(),
  			split	= number_string.split(','),
  			sisa 	= split[0].length % 3,
  			rupiah 	= split[0].substr(0, sisa),
  			ribuan 	= split[0].substr(sisa).match(/\d{3}/gi);

  		if (ribuan) {
  			separator = sisa ? '.' : '';
  			rupiah += separator + ribuan.join('.');
  		}

  		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  	}
  </script>
  <script>
/*
  document.getElementById("value").onblur =function (){
      this.value = parseFloat(this.value.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
*/
    $("#files").fileinput({
        showUpload: false,
        maxFileCount: 3,
        mainClass: "input-group-lg"
    });
  </script>

@endsection
