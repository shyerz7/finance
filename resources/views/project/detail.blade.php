@extends('layouts.master')

@section('css')
<link href="inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" href="http://plugins.krajee.com/assets/6facb0ab/css/fileinput.min.css">
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Project detail</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('project') }}">Home</a>
                </li>
                <li class="active">
                    <strong>{{ $project->name }}</strong>
                </li>
            </ol>
        </div>
    </div>

<div class="row">
    <div class="col-lg-9">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="m-b-md">
                                <a href="#" class="btn btn-white btn-xs pull-right">Edit project</a>
                                <h2>{{ $project->name }}</h2>
                            </div>
                            <dl class="dl-horizontal">
                                <dt><small>Metode:</small></dt> <dd><span class="label label-primary">{{ $project->metode_project }}</span></dd>
                            </dl>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <dl class="dl-horizontal">
                                <dt>Created By:</dt> <dd>Alvin</dd>
                                <dt>Nilai:</dt> <dd>  Rp. {{ $project->nilai }}</dd>
                                <dt>Client:</dt> <dd><a href="#" class="text-navy"> {{ $project->client_name }}</a> </dd>
                            </dl>
                        </div>
                        <div class="col-lg-7" id="cluster_info">
                            <dl class="dl-horizontal" >

                                <dt>Created:</dt> <dd> 	{{ date('j-F-Y',strtotime($project->created_at)) }} </dd>
                                <dt>Last Updated:</dt> <dd>{{ date('j-F-Y',strtotime($project->updated_at)) }}</dd>
                                <dt>Project Manager:</dt> <dd>{{ $project->project_manajer }}</dd>
                            </dl>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="wrapper wrapper-content project-manager">
            <h4>Project description</h4>
            <p class="small">
              @if(is_null($project->desc))
              There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look
              even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing
              @else
              {!! $project->desc !!}
              @endif
                </p>
            <h4>Project files</h4>
            <ul class="list-unstyled project-files">
              @foreach($projectFiles  as $projectFile)
                <li><a href="{{ route('downloadFileProject',$projectFile->id) }}"><i class="fa fa-file"></i> {{ $projectFile->nama_file }}</a></li>
              @endforeach
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="row m-t-sm">
      <div class="col-lg-12">
      <div class="panel blank-panel">
      <div class="panel-heading">
          <div class="panel-options">
              <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab-1" data-toggle="tab">Fee Project</a></li>
                  <li class=""><a href="#tab-2" data-toggle="tab">Advance Project</a></li>
              </ul>
          </div>
      </div>

      <div class="panel-body">

      <div class="tab-content">
      <div class="tab-pane active" id="tab-1">

          @if(count($feeprojects))

              <table class="table table-bordered table-form table-striped">
                  <thead>
                      <tr>
                          <th>Fee Expenses</th>
                          <th>Estimaded (Rp.)</th>
                          <th>Actual (Rp.)</th>
                          <th>Difference (Rp.)</th>
                          <th>Bukti Trf</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                          $feeCount=0;
                          $feeCountActual=0;
                          $feeCountDiff=0;
                      ?>
                      @foreach($feeprojects as $fee)
                          <tr>
                              <td>{{ $fee->fee_name }}</td>
                              <td>Rp. {{ number_format($fee->fee_estimated,0,".",".") }}</td>
                              <?php
                              $feeCount += $fee->fee_estimated;
                              ?>
                              <td>Rp. {{ number_format($fee->fee_actual,0,".",".") }}</td>
                              <?php
                              $feeCountActual += $fee->fee_actual;
                              ?>
                              <td>
                                @if($fee->fee_estimated == $fee->fee_actual)
                                  <?php
                                    $difference =  0;    
                                  ?>
                                @else
                                  <?php
                                    $difference =  $fee->fee_estimated - $fee->fee_actual;    
                                  ?>
                                @endif

                                @if($difference < 0)
                                  <?php $label = 'label label-danger' ?>
                                @else
                                  <?php $label="" ?>  
                                @endif
                                
                                <?php
                                $feeCountDiff += $difference;
                                ?>

                               
                                <span class="{{ $label }}">Rp. {{ number_format($difference,0,".",".") }}</span>  
                                
                              </td>
                              <td>
                                  @if(!is_null($fee->bukti_transfer))
                                    <?php
                                      $filesFee = json_decode($fee->bukti_transfer,true);    
                                    ?>
                                    @foreach($filesFee as $fileFee)
                                      <a href="{{ route('downloadFileFee',$fileFee) }}"><span class="label label-success">{{ $fileFee }}</span></a>   
                                    @endforeach
                                   
                                  @else
                                      belum di upload
                                  @endif
                              </td>
                              <td>
                                  <a href="{{ route('getUploadFee',$fee->id) }}" id="{{ $fee->id }}" class="btn btn-info btn-xs" data-original-title="Upload bukti transaksi" data-toggle="tooltip" data-container="body" ><i class="fa fa-upload" aria-hidden="true"></i></a>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <td><b>Total</b></td>
                          <td ><strong>Rp. {{ number_format($feeCount,0,".",".") }}</strong></td>
                          <td colspan=""><strong>Rp. {{ number_format($feeCountActual,0,".",".") }}</strong></td>
                          <td colspan="2"><strong>Rp. {{ number_format($feeCountDiff,0,".",".") }}</strong></td>
                      </tr>
                  </tfoot>
              </table>
              
              <div class="form-group">
                <a href="{{ route('updateActual',$project->id) }}" class="btn btn-success btn pull-right">Update Actual Fee</a>
              </div>

          @else
              <div class="form-group">
                <a href="{{ route('createFeeProject',$project->id) }}" class="btn btn-primary btn pull-right">Create Fee Project</a>
              </div>
          @endif

          


      </div>
      <div class="tab-pane" id="tab-2">
          @if(count($advprojects))

              <table class="table table-bordered table-form table-striped">
                  <thead>
                      <tr>
                          <th>Operations Expenses</th>
                          <th>Estimaded (Rp.)</th>
                          <th>Actual (Rp.)</th>
                          <th>Difference (Rp.)</th>
                          <th>Bukti Trf</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                          $advCount=0;
                          $advCountActual=0;
                          $advCountDiff=0;
                      ?>
                      @foreach($advprojects as $adv)
                          <tr>
                              <td>{{ $adv->adv_name }}</td>

                              <td>Rp. {{ number_format($adv->adv_estimated,0,".",".") }}</td>
                              <?php
                              $advCount += $adv->adv_estimated;
                              ?>

                              <td>Rp. {{ number_format($adv->adv_actual,0,".",".") }}</td>
                              <?php
                              $advCountActual += $adv->adv_actual;
                              ?>

                              <td>
                                @if($adv->adv_estimated == $adv->adv_actual)
                                  <?php
                                    $difference2 =  0;    
                                  ?>
                                @else
                                  <?php
                                    $difference2 =  $adv->fee_estimated - $adv->adv_actual;    
                                  ?>
                                @endif

                                @if($difference2 < 0)
                                  <?php $label = 'label label-danger' ?>
                                @else
                                  <?php $label="" ?>  
                                @endif
                                
                                <?php
                                $advCountDiff += $difference2;
                                ?>

                               
                                <span class="{{ $label }}">Rp. {{ number_format($difference2,0,".",".") }}</span>  
                                
                              </td>
                              <td>
                                  @if(!is_null($adv->bukti_transfer))
                                    <?php
                                      $filesAdv = json_decode($adv->bukti_transfer,true);    
                                    ?>
                                    @foreach($filesAdv as $fileAdv)
                                      <a href="{{ route('downloadFileAdv',$filesAdv) }}"><span class="label label-success">{{ $fileAdv }}</span></a>   
                                    @endforeach
                                   
                                  @else
                                      belum di upload
                                  @endif
                              </td>
                              <td>
                                  <a href="{{ route('getUploadAdv',$adv->id) }}" id="{{ $adv->id }}" class="btn btn-info btn-xs"><i class="fa fa-upload" aria-hidden="true"></i></a>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
                  <tfoot>
                      <tr>
                          <td><b>Total</b></td>
                          <td><strong>Rp. {{ number_format($advCount,0,".",".") }}</strong></td>
                          <td><strong>Rp. {{ number_format($advCountActual,0,".",".") }}</strong></td>
                          <td><strong>Rp. {{ number_format($advCountDiff,0,".",".") }}</strong></td>
                      </tr>
                  </tfoot>
              </table>
               <div class="form-group">
                <a href="{{ route('updateActualAdv',$project->id) }}" class="btn btn-success btn pull-right">Update Actual Advance</a>
              </div>
          @else
              <div class="form-group">
              <a href="{{ route('createAdvProject',$project->id) }}" class="btn btn-primary btn pull-right">Create Advance Project</a>
              </div>
          @endif
      </div>
      </div>

      </div>

      </div>
      </div>
  </div>
        </div>
    </div>
</div>
{{-- <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form role="form" method="post" action="{{ route('project.store') }}"  enctype="multipart/form-data">
            <div class="form-group">
                <label class="control-label">Select File</label>
                <input id="files" name="files[]" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" required="required">
            </div>
        </form>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" value="" id="hiddenVal">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> --}}

<div class="footer">
    <div class="pull-right">
        10GB of <strong>250GB</strong> Free.
    </div>
    <div>
        <strong>Copyright</strong> Example Company &copy; 2014-2017
    </div>
</div>

@endsection

@section('js')
	<script src="inspinia/js/plugins/dataTables/datatables.min.js"></script>
    <script src="http://plugins.krajee.com/assets/6facb0ab/js/fileinput.min.js"></script>
    <script>
        function feeUpload(id) {
            $('#myModalFee').modal('show');
            $('#hiddenVal').val(id);
        }
        // $("#files").fileinput({
        //     showUpload: false,
        //     maxFileCount: 3,
        //     mainClass: "input-group-lg"
        // });
    </script>
@endsection
