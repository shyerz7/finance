@extends('layouts.master')

@section('css')
<link href="inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
      <h2>List Project</h2>
      <ol class="breadcrumb">
			     <li>
            	<a href="index-2.html">Index</a>
         	</li>
      </ol>
   </div>
   <div class="col-sm-6">
        <div class="title-action">
            @if(Auth::user()->hasRole(['admin','bd']))
            <a href="{{ route('project.create') }}" class="btn btn-primary">Create new project</a>
            @endif
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-md-12">

                  {{-- Untuk Pesan Sukses --}}
                  @if(Session::has('alert-success'))
                      <div class="alert alert-success">
                            {{ Session::get('alert-success') }}
                        </div>
                  @endif

                </div>
              </div>

            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Basic Data Tables example with responsive plugin</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Project Name</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach($projects  as $project)
                            <tr>
                                <td>
                                    <strong>{{ $project->name }}</strong><br>
                                    <small>Created {{ $project->created_at }}</small>
                                </td>
                                <td>
                                    <a href="{{ route('project.detail', $project->id) }}" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> View </a>
                                    <!-- <a href="#" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> View </a> -->
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>

</div>

@endsection

@section('js')
	<script src="inspinia/js/plugins/dataTables/datatables.min.js"></script>
	<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>

@endsection
