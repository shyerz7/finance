@extends('layouts.master')

@section('css')
  <link rel="stylesheet" href="http://plugins.krajee.com/assets/6facb0ab/css/fileinput.min.css">
@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
      <h2>Upload Bukti Transaksi - {{ $fee->fee_name }}</h2>
      <ol class="breadcrumb">
  			<li>
            <a href="index-2.html">Index</a>
        </li>
        <li>
          	Transaksi - {{ $fee->fee_name }}
       	</li>
      </ol>
   </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

     <form role="form" method="post" action="{{ route('uploadFee',$fee->id) }}"  enctype="multipart/form-data">

      <div class="col-lg-12">
       <div class="ibox float-e-margins">
          <div class="ibox-title">
             <h5>Upload Bukti Transaksi<small> Fill the blank input type</small></h5>
          </div>

           {{ csrf_field() }}
          <div class="ibox-content">
             <div class="row">
                <div class="col-sm-6 b-r">


                      <div class="form-group">
                        <label class="control-label">Select File</label>
                        <input id="files" name="files[]" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" required="required">
                      </div>

                      <div>
                         <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Save</strong></button>
                         <button class="btn btn-sm btn-default pull-right m-t-n-xs" type="submit"><strong>Cancel</strong></button>
                      </div>

                </div>


             </div>

          </div>
       </div>
    </div>

    </form>

</div>

@endsection

@section('js')
  <script src="http://plugins.krajee.com/assets/6facb0ab/js/fileinput.min.js"></script>
  <script type="text/javascript">

    $("#files").fileinput({
        showUpload: false,
        maxFileCount: 3,
        mainClass: "input-group-lg"
    });
  </script>

@endsection
