@extends('layouts.master')

@section('css')
@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
      <h2>Create Fee Project</h2>
      <ol class="breadcrumb">
  			<ol class="breadcrumb">
            <li>
                <a href="{{ route('project') }}">Home</a>
            </li>
            <li>
                <a href="{{ route('project.detail',$project->detail) }}">{{ $project->name }}</a>
            </li>
            <li class="active">
                <strong>Create Fee Project</strong>
            </li>
        </ol>
      </ol>
   </div>
</div>

<div class="row" id="fee">
    <div class="col-lg-9">
        <div class="wrapper wrapper-content animated fadeInUp">
          <div class="ibox">
                <div class="ibox-content">
                  <p class="lead">Budget yang disediakan <strong>Rp. {{ ($project->nilai * 60) / 100  }}</strong> </p>
                  <table class="table table-bordered table-form">
                    <thead>
                        <tr>
                            <th>Fee Expenses</th>
                            <th>Estimaded (Rp.)</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                      <tr v-for="fee in form.fees"> 
                        <td class="table-fee_name" :class="{'table-error': errors['fees.' + $index + '.fee_name']}">
                            <input type="hidden"   v-model="fee.fee_name">
                            <input type="text" class="form-control"  v-model="fee.fee_name">
                        </td>
                        <td class="table-fee_estimated" :class="{'table-error': errors['fees.' + $index + '.fee_estimated']}">
                            <input type="text" class="form-control"  v-model="fee.fee_estimated" id="fee_estimated">
                        </td>
                        <td>
                          <label for="" class="label-primary">
                            <span @click="remove(fee)" class="table-remove-btn">&times;</span>
                          </label>
                        </td>    
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                          <td class="table-empty" colspan="1">
                              <span @click="addLine" class="table-add_line label label-primary">Tambah Fee</span>
                          </td>
                          <td class="table-amount">Total : @{{subTotal}}</td>
                      </tr>
                    </tfoot> 
                  </table>

                  <a href="{{route('project.detail',$project->id)}}" class="btn btn-default">Batal</a>
                  <button class="btn btn-success" @click="create" :disabled="isProcessing">Simpan</button>
                  
                </div>
          </div>      
        </div>
    </div>    
</div>

@endsection

@section('js')
<script src="{{ asset('/js/vue.min.js') }}"></script>
<script src="{{ asset('/js/vue-resource.min.js') }}"></script>
<script type="text/javascript">
    Vue.http.headers.common['X-CSRF-TOKEN'] = '{{csrf_token()}}';
     window.project_id = {{ $project->id }}
    window._form = {
        project_id : {{ $project->id }},
        fees: [{
            project_id:{{ $project->id }},
            fee_name: '',
            fee_estimated: 0
        }]
    };
</script>
<script src="{{ asset('/js/app.js') }}"></script>
{{-- <script type="text/javascript">

    /* Tanpa Rupiah */
    var tanpa_rupiah = document.getElementById('fee_estimated');
    tanpa_rupiah.addEventListener('keyup', function(e)
    {
      tanpa_rupiah.value = formatRupiah(this.value);
    });

    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
  </script> --}}
@endsection
