@extends('layouts.master')

@section('css')
@endsection

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
      <h2>Create Fee Project</h2>
      <ol class="breadcrumb">
  			<ol class="breadcrumb">
            <li>
                <a href="{{ route('project') }}">Home</a>
            </li>
            <li>
                <a href="{{ route('project.detail',$project->detail) }}">{{ $project->name }}</a>
            </li>
            <li class="active">
                <strong>Update Actual Fee</strong>
            </li>
        </ol>
      </ol>
   </div>
</div>

<div class="row" id="fee">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
          <div class="ibox">
                <div class="ibox-content">
                  <table class="table table-bordered table-form">
                    <thead>
                        <tr>
                            <th>Fee Expenses</th>
                            <th>Estimaded (Rp.)</th>
                            <th>Actual</th>
                        </tr>
                    </thead>
                    <tbody>

                      <tr v-for="fee in form.fees"> 
                        <td class="table-fee_name" :class="{'table-error': errors['fees.' + $index + '.fee_name']}">
                            <input type="text" class="form-control"  v-model="fee.fee_name">
                        </td>
                        <td class="table-fee_estimated" :class="{'table-error': errors['fees.' + $index + '.fee_estimated']}">
                            <input type="text" class="form-control"  v-model="fee.fee_estimated" readonly>
                        </td>
                        <td class="table-fee_actual" :class="{'table-error': errors['fees.' + $index + '.fee_actual']}">
                            <input type="text" class="form-control"  v-model="fee.fee_actual" id="fee_actual">
                        </td> 
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                          <td class="table-empty" colspan="1">
                              <span @click="addLine" class="table-add_line label label-primary">Tambah Fee</span>
                          </td>
                          <td class="table-amount">Total : @{{subTotal}}</td>
                          <td class="table-amount">Total : @{{actualTotal}}</td>
                      </tr>
                    </tfoot> 
                  </table>

                  <a href="{{route('project.detail',$project->id)}}" class="btn btn-default">Batal</a>
                  <button class="btn btn-success" @click="updateActual" :disabled="isProcessing">Simpan</button>
                </div>
          </div>      
        </div>
    </div>    
</div>

@endsection

@section('js')
<script src="{{ asset('/js/vue.min.js') }}"></script>
<script src="{{ asset('/js/vue-resource.min.js') }}"></script>
<script type="text/javascript">
    Vue.http.headers.common['X-CSRF-TOKEN'] = '{{csrf_token()}}';

    window._form = {!! $project->toJson() !!};
    window.project_id = {{ $project->id }}
</script>
<script src="{{ asset('/js/app.js') }}"></script>
{{-- <script type="text/javascript">

    /* Tanpa Rupiah */
    var tanpa_rupiah = document.getElementById('fee_estimated');
    tanpa_rupiah.addEventListener('keyup', function(e)
    {
      tanpa_rupiah.value = formatRupiah(this.value);
    });

    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa  = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
  </script> --}}
@endsection
