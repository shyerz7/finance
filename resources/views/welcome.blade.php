
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>RAD Research</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 80vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 70px;
            }

            .links > a {
                color: #ffffff;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .ghost-button {
                display: inline-block;
                width: 200px;
                padding: 8px;
                border: 1px solid #ffffff;
                text-align: center;
                outline: none;
                text-decoration: none;
                line-height: 40px;
                border-radius: 40px;
              }
              body {
              color: #ffffff;
              background: #f09433;
              background: -moz-linear-gradient(45deg, #f09433 0%, #e6683c 25%, #dc2743 50%, #cc2366 75%, #bc1888 100%);
              background: -webkit-linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
              background: linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
              filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f09433', endColorstr='#bc1888',GradientType=1 );

              background-size: 200% 200%;

              -webkit-animation: AnimationName 10s ease infinite;
              -moz-animation: AnimationName 10s ease infinite;
              animation: AnimationName 10s ease infinite;
                }

              @-webkit-keyframes AnimationName {
                  0%{background-position:0% 91%}
                  50%{background-position:100% 10%}
                  100%{background-position:0% 91%}
              }
              @-moz-keyframes AnimationName {
                  0%{background-position:0% 91%}
                  50%{background-position:100% 10%}
                  100%{background-position:0% 91%}
              }
              @keyframes AnimationName {
                  0%{background-position:0% 91%}
                  50%{background-position:100% 10%}
                  100%{background-position:0% 91%}
              }
              .instagram{
                width:100px; height:100px;
                background: #f09433;
                background: -moz-linear-gradient(45deg, #f09433 0%, #e6683c 25%, #dc2743 50%, #cc2366 75%, #bc1888 100%);
                background: -webkit-linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
                background: linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f09433', endColorstr='#bc1888',GradientType=1 );
                }
        </style>
    </head>
    <body class="">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md" style="text-align: center;line-height: 100px">
                  <div class="padding-top:30px;"><img src="{{ asset('img/wallet.png') }}" style="height: 125px;"></div>
                    <img src="{{ asset('img/rad-white.png') }}" >&nbsp;Finance Module
                </div>

                <div class="links">
                  <a href="{{ url('/login') }}" class="ghost-button">Login</a>
                </div>
            </div>
        </div>
    </body>
</html>
