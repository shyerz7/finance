<nav class="navbar-default navbar-static-side" role="navigation">
   <div class="sidebar-collapse">
      <ul class="nav metismenu" id="side-menu">

         <li class="nav-header">
            @include('partials.profilemenu')
         </li>

         {{-- Menu untuk BD --}}
         <li>
            <a href="{{URL::to('/home')}}"><i class="fa fa-tachometer"></i> <span class="nav-label">Dashboard </span></a>
         </li>
         <li>
            <a href="{{URL::to('/project')}}"><i class="fa fa-tasks"></i> <span class="nav-label">Project</span><span class="label label-warning pull-right">16/24</span></a>
         </li>
         {{-- <li>
            <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">Inbox</span><span class="label label-warning pull-right">16/24</span></a>
         </li> --}}
      </ul>
   </div>
</nav>
