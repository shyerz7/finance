@extends('layouts.master')

@section('content')
	
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
      <h2>Dashboard Page Bussiness Development</h2>
      <ol class="breadcrumb">
			<li>
            	<a href="index-2.html">Dashboard</a>
         	</li>
      </ol>
   </div>
</div>	

<div class="wrapper wrapper-content">
	
	<div class="row">

	   <div class="col-lg-3">
	      <div class="ibox float-e-margins">
	         <div class="ibox-title">
	            <span class="label label-success pull-right">This Year</span>
	            <h5>Project Value</h5>
	         </div>
	         <div class="ibox-content">
	            <h1 class="no-margins">Rp. 40 886,200</h1>
	            <small>Project Value This Year</small>
	         </div>
	      </div>
	   </div>

	   <div class="col-lg-3">
	      <div class="ibox float-e-margins">
	         <div class="ibox-title">
	            <span class="label label-info pull-right">This Month</span>
	            <h5>Project Value</h5>
	         </div>
	         <div class="ibox-content">
	            <h1 class="no-margins">Rp. 275,800</h1>
	            <small>Project Value This Month</small>
	         </div>
	      </div>
	   </div>

	   <div class="col-lg-3">
	      <div class="ibox float-e-margins">
	         <div class="ibox-title">
	            <span class="label label-primary pull-right">This Year</span>
	            <h5>Total Project</h5>
	         </div>
	         <div class="ibox-content">
	            <h1 class="no-margins">78</h1>
	            <small>Total Project This Year</small>
	         </div>
	      </div>
	   </div>

	   <div class="col-lg-3">
	      <div class="ibox float-e-margins">
	         <div class="ibox-title">
	            <span class="label label-danger pull-right">This Month</span>
	            <h5>Total Project</h5>
	         </div>
	         <div class="ibox-content">
	            <h1 class="no-margins">10</h1>
	            {{-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div> --}}
	            <small>Total Project This Month</small>
	         </div>
	      </div>
	   </div>

	</div>

	<div class="row">
		
		<div class="col-md-12">
			<div class="ibox float-e-margins">
			   <div class="ibox-title">
			      <h5>Bar Chart Project Value This Year <small></small></h5>
			      <div class="ibox-tools">
			         <a class="collapse-link">
			         <i class="fa fa-chevron-up"></i>
			         </a>
			         <a class="dropdown-toggle" data-toggle="dropdown" href="#">
			         <i class="fa fa-wrench"></i>
			         </a>
			         <ul class="dropdown-menu dropdown-user">
			            <li><a href="#">Config option 1</a>
			            </li>
			            <li><a href="#">Config option 2</a>
			            </li>
			         </ul>
			         <a class="close-link">
			         <i class="fa fa-times"></i>
			         </a>
			      </div>
			   </div>
			   <div class="ibox-content" style="position: relative">
			      
			      <div id="q17-chart" style="min-width: 310px; width:auto;height: 380px; margin: 0 auto"></div>


			   </div>
			</div>
		</div>

	</div>

</div>

@endsection

@section('js')
	
	<script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

	<script>
		$(function () {

			$('#q17-chart').highcharts({
               chart: {
                    type: 'column'
                },
                colors: ['#3498DB'],
                title: {
                    text: 'Rank ',
                    style: {
                        fontWeight: 'bold',
                        fontSize:'20px'
                    }
                },
                xAxis: {
                    type: 'category',
                     title: {
                        
                    },
                    labels: {

                        style: {
                            fontSize: '14px',
                            fontFamily: 'Verdana, sans-serif',

                        }
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                fontSize:'18px'
                            }
                        },
                        
                    }
                },
                yAxis: {
                     // tickPositioner: function() {
                     //        return [0,10];
                     //    },
                     gridLineWidth: 0,
                    
                    title: {
                        text: 'Persentase'
                    }
                    
                },
                credits:{
                    enabled:false
                },

                series: [{
                    name: 'Score',
                    colorByPoint: true,
                    data: [{"name":"Sangat Tidak Puas","y":0.3},{"name":"Tidak Puas","y":1.1},{"name":"Biasa Saja","y":6.7},{"name":"Puas","y":50},{"name":"Sangat Puas","y":41.9}]   
                }],        
            }); 



		});		
	</script>
@endsection