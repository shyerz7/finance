@extends('layouts.app')
@section('content')
<div class="container">
        <div class="wrapper wrapper-content middle-box text-center loginscreen animated fadeInDown" style="background-color: #ffffff;">
          <img src="{{ asset('img/rad.png') }}" style="max-width: 200px;" alt="">
          <br /><br />
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <div class="checkbox">

                                    <label>
                                      Remember Me
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-danger btn-block">
                                    Login
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                  <small>Forgot Your Password?</small>
                              </a>
                            </div>
                        </div>
                        <p></p><br />
        </div>
      </div>
@endsection
