<!DOCTYPE html>
<html>
   <!-- Mirrored from webapplayers.com/inspinia_admin-v2.7/empty_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 16 Mar 2017 05:19:39 GMT -->
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>RAD Finance</title>
      <link href="{{ asset('inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
      <link href="{{ asset('inspinia/css/animate.css') }}" rel="stylesheet">
      <link href="{{ asset('inspinia/css/style.css') }}" rel="stylesheet">

      @yield('css')
   </head>

   <body class="">
      <div id="wrapper">

        @include('partials.sidebar')

        <div id="page-wrapper" class="gray-bg">

            <div class="row border-bottom">
               @include('partials.header')
            </div>

            <div id="content">

                <!-- <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-sm-4">
                      <h2>This is main title</h2>
                      <ol class="breadcrumb">
                         <li>
                            <a href="index-2.html">This is</a>
                         </li>
                         <li class="active">
                            <strong>Breadcrumb</strong>
                         </li>
                      </ol>
                   </div>
                   <div class="col-sm-8">
                      <div class="title-action">
                         <a href="#" class="btn btn-primary">This is action area</a>
                      </div>
                   </div>
                </div>

                <div class="wrapper wrapper-content">
                   <div class="middle-box text-center animated fadeInRightBig">
                      <h3 class="font-bold">This is page content</h3>
                      <div class="error-desc">
                         You can create here any grid layout you want. And any variation layout you imagine:) Check out
                         main dashboard and other site. It use many different layout.
                         <br/><a href="index-2.html" class="btn btn-primary m-t">Dashboard</a>
                      </div>
                   </div>
                </div> -->

                @yield('content')

            </div>


            {{-- @include('partials.footer') --}}

         </div>
      </div>
      <!-- Mainly scripts -->
      <script src="{{ asset('inspinia/js/jquery-3.1.1.min.js') }}"></script>
      <script src="{{ asset('inspinia/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
      <script src="{{ asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
      <!-- Custom and plugin javascript -->
      <script src="{{ asset('inspinia/js/inspinia.js') }}"></script>
      <script src="{{ asset('inspinia/js/plugins/pace/pace.min.js') }}"></script>

      @yield('js')

   </body>
</html>
