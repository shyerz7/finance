<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeProject extends Model
{
    protected $table = 'free_projects';

    protected $fillable = [
    	'project_id',
    	'fee_name',
    	'fee_estimated',
    	'fee_actual',
    	'fee_difference',
    	'bukti_transfer',
    ];

    public function project()
    {
    	return $this->belongsTo('App\Project','project_id');
    }
}
