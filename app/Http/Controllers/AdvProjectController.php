<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\AdvanceProject;

class AdvProjectController extends Controller
{
    public function createAdvProject($id_project)
    {
    	$project = Project::findOrFail($id_project);

    	return view('adv.create',compact('project'));
    }

    public function saveAdvProject(Request $request,$id_project)
    {

    	$project = Project::findOrFail($id_project);

    	$fees = collect($request->advances)->transform(function($advance){
    		return AdvanceProject::create($advance);
    	});

    	if($fees->isEmpty()) {
            return response()
            ->json([
                'advances_empty' => ['One or more Product is required.']
            ], 422);
        }

        return response()
            ->json([
                'created' => true,
                'id' => $project->id
            ]);
    }

    public function getUploadAdv($id_fee)
    {
        $adv = AdvanceProject::findOrFail($id_fee);
        return view('adv.uploadAdvFile',compact('adv'));

    }

    public function uploadAdv(Request $request,$id_fee)
    {
        $adv = AdvanceProject::findOrFail($id_fee);
        if($request->hasFile('files'))
        {
            // Request the file input named 'attachments'
            $files = $request->file('files');

            $datafiles = [];

            foreach($files as $file) {
                // Get the orginal filname or create the filename of your choice
                $data          = $file->getClientOriginalName();
                $filenameOri   = pathinfo($data,PATHINFO_FILENAME);

                $extension = $file->getClientOriginalExtension();

                $path = storage_path(). DIRECTORY_SEPARATOR .'app'.DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR .'advfiles';


                $filename = substr( md5(rand()), 0, 3).'-'.$filenameOri . '.' . $extension;


                $file->move($path,$filename);

                array_push($datafiles, $filename);

            }

            $adv->bukti_transfer = json_encode($datafiles);
            $adv->save();
        }

        return redirect()->route('project.detail',$adv->project_id);
    }

    public function updateActualAdv($id_project)
    {
        $project = Project::with('advances')->findOrFail($id_project);
        $advances = AdvanceProject::where('project_id')->get();

        return view('adv.updateActual',compact('project','advances'));

    }

    public function updateActualAdvStore(Request $request,$id_project)
    {

        $project = Project::findOrFail($id_project);

        $advances = collect($request->advances)->transform(function($advance) use ($id_project){

            if(isset($advance['id']))
            {
                $val = AdvanceProject::findOrFail($advance['id']);
                $val->adv_actual    =   $advance['adv_actual'];
                return $val->save();

            }else{
                $advance['project_id']  = $id_project;
                return AdvanceProject::create($advance);  
            }
            
        });

        if($advances->isEmpty()) {
            return response()
            ->json([
                'fees_empty' => ['One or more Product is required.']
            ], 422);
        }

        // return redirect()->route('project.detail',$project->id);

        return response()
            ->json([
                'updated' => true,
                'id' => $project->id
            ]);
    }

    public function downloadFileAdv($nama_file)
    {
        $path = storage_path(). DIRECTORY_SEPARATOR .'app'.DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR .'advfiles'.DIRECTORY_SEPARATOR .$nama_file;

        return response()->download($path);
    }
}
