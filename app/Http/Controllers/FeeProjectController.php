<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\FeeProject;

class FeeProjectController extends Controller
{
    public function create($id_project)
    {
    	$project = Project::findOrFail($id_project);

    	return view('fee.create',compact('project'));
    }

    public function saveFeeProject(Request $request,$id_project)
    {

    	$project = Project::findOrFail($id_project);

    	$fees = collect($request->fees)->transform(function($fee){
    		return FeeProject::create($fee);
    	});

    	if($fees->isEmpty()) {
            return response()
            ->json([
                'fees_empty' => ['One or more Product is required.']
            ], 422);
        }

        return response()
            ->json([
                'created' => true,
                'id' => $project->id
            ]);
    }

    public function getUploadFee($id_fee)
    {
        $fee = FeeProject::findOrFail($id_fee);
        return view('fee.uploadFeeFile',compact('fee'));

    }

    public function uploadFee(Request $request,$id_fee)
    {
        $fee = FeeProject::findOrFail($id_fee);
        if($request->hasFile('files'))
        {
            // Request the file input named 'attachments'
            $files = $request->file('files');

            $datafiles = [];

            foreach($files as $file) {
                // Get the orginal filname or create the filename of your choice
                $data          = $file->getClientOriginalName();
                $filenameOri   = pathinfo($data,PATHINFO_FILENAME);

                $extension = $file->getClientOriginalExtension();

                $path = storage_path(). DIRECTORY_SEPARATOR .'app'.DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR .'feefiles';


                $filename = substr( md5(rand()), 0, 3).'-'.$filenameOri . '.' . $extension;


                $file->move($path,$filename);

                array_push($datafiles, $filename);

            }

            $fee->bukti_transfer = json_encode($datafiles);
            $fee->save();
        }

        return redirect()->route('project.detail',$fee->project_id);
    }

    public function updateActual($id_project)
    {
        $project = Project::with('fees')->findOrFail($id_project);
        $fees = FeeProject::where('project_id')->get();

        return view('fee.updateActual',compact('project','fees'));

    }

    public function updateActualStore(Request $request,$id_project)
    {

        $project = Project::findOrFail($id_project);

        $fees = collect($request->fees)->transform(function($fee) use ($id_project){

            if(isset($fee['id']))
            {
                $val = FeeProject::findOrFail($fee['id']);
                $val->fee_actual    =   $fee['fee_actual'];
                return $val->save();

            }else{
                $fee['project_id']  = $id_project;
                return FeeProject::create($fee);  
            }
            
        });

        if($fees->isEmpty()) {
            return response()
            ->json([
                'fees_empty' => ['One or more Product is required.']
            ], 422);
        }

        // return redirect()->route('project.detail',$project->id);

        return response()
            ->json([
                'updated' => true,
                'id' => $project->id
            ]);
    }

    public function downloadFileFee($nama_file)
    {
        $path = storage_path(). DIRECTORY_SEPARATOR .'app'.DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR .'feefiles'.DIRECTORY_SEPARATOR .$nama_file;

        return response()->download($path);
    }
}
