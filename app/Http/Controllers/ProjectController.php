<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\ProjectFile;
use App\FeeProject;
use App\AdvanceProject;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class ProjectController extends Controller
{
    public function index() {
    	$projects = Project::with('fees')->get();
    	return view('project.index',compact('projects'));

    }

    public function create() {
    	return view('project.create');
    }

    public function detail($id) {
        $projectFiles = ProjectFile::where('id_project', $id)->get();
        $project = Project::findOrFail($id);
        $feeprojects = FeeProject::where('project_id',$project->id)->get();
        $advprojects = AdvanceProject::where('project_id',$project->id)->get();
        return view('project.detail',compact('project','projectFiles','feeprojects','advprojects'));
    }

    public function store(Request $request)
    {
        $user = \Auth::user();
        $data['name']               =   $request->input('nama_project');
        $data['created_at']         =   Carbon::parse($request->input('created_at'))->format('Y-m-d H:i:s');
        $data['client_name']        =   $request->input('client_name');
        $data['client_contact']     =   $request->input('client_contact');
        $data['project_manajer']    =   $request->input('project_manajer');
    	$data['metode_project']	    =	$request->input('metode_project');
        $data['nilai']              =   str_replace(".","",$request->input('nilai_project'));
        $data['desc']               =   $request->input('desc');
    	$data['created_by']	        =	$user->id;
    	$project                    =   Project::create($data);


    	if($request->hasFile('files'))
        {
        	// Request the file input named 'attachments'
            $files = $request->file('files');



        	foreach($files as $file) {
        		// Get the orginal filname or create the filename of your choice
                $data          = $file->getClientOriginalName();
                $filenameOri   = pathinfo($data,PATHINFO_FILENAME);

                $extension = $file->getClientOriginalExtension();

                $path = storage_path(). DIRECTORY_SEPARATOR .'app'.DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR .'projectfiles';


                $filename = substr( md5(rand()), 0, 3).'-'.$filenameOri . '.' . $extension;


                $file->move($path,$filename);

                $val['id_project']	=	$project->id;
                $val['nama_file']	=   $filename;
                $fileProject	=	ProjectFile::create($val);

        	}
        }

        return redirect()->route('project')->with('alert-success','Data Project Sudah diupload');

    }

    public function downloadFileProject($id_file)
    {
        $file = ProjectFile::findOrFail($id_file);
        $path = storage_path(). DIRECTORY_SEPARATOR .'app'.DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR .'projectfiles'.DIRECTORY_SEPARATOR .$file->nama_file;

        return response()->download($path);

    }

    
}
