<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
    	'name',
    	'nilai',
    	'client_name',
    	'desc',
    	'client_contact',
    	'project_manajer',
    	'metode_project',
    	'created_by',
    ];

    public function user () {
    	return $this->belongsTo('App\Project','created_by');
    }

    public function fees()
    {
        return $this->hasMany('App\FeeProject');
    }

    public function advances()
    {
        return $this->hasMany('App\AdvanceProject');
    }
}
