<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvanceProject extends Model
{
    protected $table = 'advance_projects';

    protected $fillable = [
    	'project_id',
    	'adv_name',
    	'adv_estimated',
    	'adv_actual',
    	'adv_difference',
    	'bukti_transfer',
    ];

    public function project()
    {
    	return $this->belongsTo('App\Project','project_id');
    }
}
