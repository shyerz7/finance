<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


/**
 * route project
 */
Route::get('/project',[
	'as'	=>	'project',
	'uses'	=>	'ProjectController@index'
]);
Route::get('/project/create',[
	'as'	=>	'project.create',
	'uses'	=>	'ProjectController@create'
]);
Route::post('/project/store',[
	'as'	=>	'project.store',
	'uses'	=>	'ProjectController@store'
]);
Route::get('/project/detail/{id}',[
	'as'	=>	'project.detail',
	'uses'	=>	'ProjectController@detail'
]);

/**
 * route fee project
 */
Route::get('/createFeeProject/{id_project}',[
	'as'	=>	'createFeeProject',
	'uses'	=>	'FeeProjectController@create'
]);
Route::post('/saveFeeProject/{id_project}',[
	'as'	=>	'saveFeeProject',
	'uses'	=>	'FeeProjectController@saveFeeProject'
]);

/**
 * route adv project
 */
Route::get('/createAdvProject/{id_project}',[
	'as'	=>	'createAdvProject',
	'uses'	=>	'AdvProjectController@createAdvProject'
]);
Route::post('/saveAdvProject/{id_project}',[
	'as'	=>	'saveAdvProject',
	'uses'	=>	'AdvProjectController@saveAdvProject'
]);

/**
 * download use
 */
Route::get('/downloadFileProject/{id}',[
	'as'	=>	'downloadFileProject',
	'uses'	=>	'ProjectController@downloadFileProject'
]);

/**
 * upload buktransaksi fee 
 */
Route::get('/getUploadFee/{id_fee}',[
	'as'	=>	'getUploadFee',
	'uses'	=>	'FeeProjectController@getUploadFee'
]);
Route::post('/uploadFee/{id_fee}',[
	'as'	=>	'uploadFee',
	'uses'	=>	'FeeProjectController@uploadFee'
]);
Route::get('/updateActual/{id_project}',[
	'as'	=>	'updateActual',
	'uses'	=>	'FeeProjectController@updateActual'
]);
Route::post('/updateActual/{id_fee}',[
	'as'	=>	'updateActual',
	'uses'	=>	'FeeProjectController@updateActualStore'
]);
Route::get('/downloadFileFee/{nama_file}',[
	'as'	=>	'downloadFileFee',
	'uses'	=>	'FeeProjectController@downloadFileFee'
]);

/**
 * upload buktransaksi adv
 */
Route::get('/getUploadAdv/{id_fee}',[
	'as'	=>	'getUploadAdv',
	'uses'	=>	'AdvProjectController@getUploadAdv'
]);
Route::post('/uploadAdv/{id_fee}',[
	'as'	=>	'uploadAdv',
	'uses'	=>	'AdvProjectController@uploadAdv'
]);
Route::get('/updateActualAdv/{id_project}',[
	'as'	=>	'updateActualAdv',
	'uses'	=>	'AdvProjectController@updateActualAdv'
]);
Route::post('/updateActualAdv/{id_project}',[
	'as'	=>	'updateActualAdv',
	'uses'	=>	'AdvProjectController@updateActualAdvStore'
]);
Route::get('/downloadFileAdv/{nama_file}',[
	'as'	=>	'downloadFileAdv',
	'uses'	=>	'AdvProjectController@downloadFileAdv'
]);
