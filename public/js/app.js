var app = new Vue({
  el: '#fee',
  data: {
    isProcessing: false,
    form: {},
    errors: {},
    project_id
  },
  created: function () {
    Vue.set(this.$data, 'form', _form);
    Vue.set(this.$data, 'project_id', project_id);
    console.log(this.$data.form);
    console.log("project id" + this.project_id);
  },
  // ready:function() {
  //   this.fetchFees();
  // }
  methods: {
    fetchFees:function() {
      Vue.set(this.$data, 'form', _form);
      console.log(this.$data);
    },
    addLine: function() {
      this.form.fees.push({project_id:this.form.project_id,fee_name: '', fee_estimated: 0, fee_actual: 0,fee_difference: 0});
    },
    remove: function(fee) {
      this.form.fees.$remove(fee);
    },
    create: function() {
      this.isProcessing = true;
      this.$http.post('/finance_div2/public/saveFeeProject/'+this.form.project_id, this.form)
        .then(function(response) {
          if(response.data.created) {
            window.
            location = '/finance_div2/public/project/detail/' + response.data.id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
        })
    },
    updateActual: function() {
      this.isProcessing = true;
      this.$http.post('/finance_div2/public/updateActual/' + this.project_id,this.form)
        .then(function(response) {
          if(response.data.updated) {
            window.location = '/finance_div2/public/project/detail/' + response.data.id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
        })
    }
  },
  computed: {
    subTotal: function() {
      return this.form.fees.reduce(function(carry, fee) {
        return carry + (parseFloat(fee.fee_estimated));
      }, 0);
    },
    actualTotal: function() {
      return this.form.fees.reduce(function(carry, fee) {
        return carry + (parseFloat(fee.fee_actual));
      }, 0);
    },
    grandTotal: function() {
      return this.subTotal - parseFloat(this.form.discount);
    },
    differenceCount: function(estimated,actual) {
      return estimated - actual;
    }
  }
})

var app2 = new Vue({
  el: '#advance',
  data: {
    isProcessing: false,
    form: {},
    errors: {},
    project_id
  },
  created: function () {
    Vue.set(this.$data, 'form', _form);
    Vue.set(this.$data, 'project_id', project_id);
  },
  methods: {
    addLine: function() {
      this.form.advances.push({project_id:this.form.project_id,adv_name: '', adv_estimated: 0,adv_actual: 0});
    },
    remove: function(advance) {
      this.form.advances.$remove(advance);
    },
    create: function() {
      this.isProcessing = true;
      this.$http.post('/finance_div2/public/saveAdvProject/'+this.form.project_id, this.form)
        .then(function(response) {
          if(response.data.created) {
            window.location = '/finance_div2/public/project/detail/' + response.data.id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
        })
    },
    updateActual: function() {
      this.isProcessing = true;
      this.$http.post('/finance_div2/public/updateActualAdv/' + this.form.id, this.form)
        .then(function(response) {
          if(response.data.updated) {
            window.location = '/finance_div2/public/project/detail/' + response.data.id;
          } else {
            this.isProcessing = false;
          }
        })
        .catch(function(response) {
          this.isProcessing = false;
          Vue.set(this.$data, 'errors', response.data);
        })
    }
  },
  computed: {
    subTotal: function() {
      return this.form.advances.reduce(function(carry, advance) {
        return carry + (parseFloat(advance.adv_estimated));
      }, 0);
    },
    actualTotal: function() {
      return this.form.advances.reduce(function(carry, advance) {
        return carry + (parseFloat(advance.adv_actual));
      }, 0);
    },
    grandTotal: function() {
      return this.subTotal - parseFloat(this.form.discount);
    }
  }
})
